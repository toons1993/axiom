package axiom.com.example.axiom;

import org.springframework.lang.NonNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Release {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @NonNull
    private Long releaseId;

    @NonNull
    private String announceDate;

    @NonNull
    private Integer priceEur;

    @NonNull
    public Long getId() {
        return releaseId;
    }

    public void setId(@NonNull Long releaseId) {
        this.releaseId = releaseId;
    }

    @NonNull
    public String getAnnounceDate() {
        return announceDate;
    }

    public void setAnnounceDate(@NonNull String announceDate) {
        this.announceDate = announceDate;
    }

    @NonNull
    public Integer getPriceEur() {
        return priceEur;
    }

    public void setPriceEur(@NonNull Integer priceEur) {
        this.priceEur = priceEur;
    }
}
