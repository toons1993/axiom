package axiom.com.example.axiom;

public class SearchCriteria {
    private String key;
    private Operation operation;
    private Object value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public SearchCriteria(String key, Operation operation, Object value) {
        this.key = key;
        this.operation = operation;
        this.value = value;
    }
}
