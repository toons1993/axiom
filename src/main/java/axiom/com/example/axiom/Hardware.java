package axiom.com.example.axiom;

import org.springframework.lang.NonNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Hardware {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @NonNull
    private Long hardwareId;

    @NonNull
    private String audioJack;

    @NonNull
    private String gps;

    @NonNull
    private String battery;

    @NonNull
    public Long getId() {
        return hardwareId;
    }

    public void setId(Long hardwareId) {
        this.hardwareId = hardwareId;
    }

    @NonNull
    public String getAudioJack() {
        return audioJack;
    }

    public void setAudioJack(@NonNull String audioJack) {
        this.audioJack = audioJack;
    }

    @NonNull
    public String getGps() {
        return gps;
    }

    public void setGps(@NonNull String gps) {
        this.gps = gps;
    }

    @NonNull
    public String getBattery() {
        return battery;
    }

    public void setBattery(@NonNull String battery) {
        this.battery = battery;
    }
}

