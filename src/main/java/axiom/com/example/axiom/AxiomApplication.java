package axiom.com.example.axiom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AxiomApplication {

	public static void main(String[] args) {
		SpringApplication.run(AxiomApplication.class, args);
	}

}
