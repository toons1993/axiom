package axiom.com.example.axiom;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class SerializationService {

    private ObjectMapper mapper;

    public SerializationService() {
        this.mapper = new ObjectMapper();
    }


    public List<Mobile> fromJson(String data) throws IOException {
        List<Mobile> response = Arrays.asList(mapper.readValue(data, Mobile[].class));
        return response;
    }
}
