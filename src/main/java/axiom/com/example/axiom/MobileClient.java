package axiom.com.example.axiom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;

@Service
public class MobileClient {

    private static List<Mobile> mobiles;

    private SerializationService serializationService;
    @Autowired
    private MobileRepository mobileRepository;

    private final Map<String, String> responses = new HashMap<>();

    @PostConstruct
    public void init() throws IOException {

        List<String> filenames = Arrays.asList(
                "AllMobiles.json"
        );
        for (String filename : filenames) {
            initResponseMocks(filename);
        }
        this.mobiles = serializationService.fromJson(responses.get("AllMobiles.json"));
        mobiles.forEach(m -> {
            mobileRepository.save(m);
        });
    }

    public MobileClient(SerializationService serializationService) throws IOException {
        this.serializationService = serializationService;
    }

    private void initResponseMocks(final String fileName) {
        try {
            Path path = Paths.get(requireNonNull(MobileClient.class.getClassLoader()
                    .getResource(fileName)).toURI());
            Stream<String> lines = Files.lines(path);
            String data = lines.collect(Collectors.joining());
            lines.close();
            responses.put(fileName, data);
        } catch (Exception e) {
        }
    }

    public List<Mobile> getMobiles(Map<String, String> allFilter) throws IOException {
        try {
            List<Specification<Mobile>> finalSpec = new ArrayList<>();

            allFilter.entrySet().forEach(filter -> {
                if (checkFilterKey(filter.getKey())) {
                    if (doesObjectContainField(Mobile.class, filter.getKey())) {
                        MobileSpecification specMobile = new MobileSpecification(new SearchCriteria(filter.getKey(), Operation.MOBILE, filter.getValue()));
                        finalSpec.add(Specification.where(specMobile));
                    }
                    if (doesObjectContainField(Hardware.class, filter.getKey())) {
                        MobileSpecification specMobile = new MobileSpecification(new SearchCriteria(filter.getKey(), Operation.HARDWARE, filter.getValue()));
                        finalSpec.add(Specification.where(specMobile));
                    }
                    if (doesObjectContainField(Release.class, filter.getKey()) || filter.getKey().equals("price")) {
                        MobileSpecification specMobile = new MobileSpecification(new SearchCriteria(filter.getKey(), Operation.RELEASE, filter.getValue()));
                        finalSpec.add(Specification.where(specMobile));
                    }
                } else {
                    throw new IllegalArgumentException("Invalid Key: " + filter.getKey());
                }
            });

            Specification<Mobile> reduce = null;
            if (finalSpec.size() != 0)
            reduce = finalSpec.stream().reduce((a, b) -> a.and(b)).get();

            return mobileRepository.findAll(reduce);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Something went wrong");
        }
    }

    public Boolean checkFilterKey(String key) {
        for (Filter filter : Filter.values()) {
            if (key.equalsIgnoreCase(filter.name()))
                return true;
        }
        return false;
    }

    public boolean doesObjectContainField(Class<?> objectClass, String fieldName) {
        try {
            objectClass.getDeclaredField(fieldName);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

}
