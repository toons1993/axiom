package axiom.com.example.axiom;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
public class MobileController {

    private MobileClient mobileClient;

    public MobileController(MobileClient mobileClient) {
        this.mobileClient = mobileClient;
    }

    @GetMapping("/mobile/search")
    public List<Mobile> getMobileSearch(@RequestParam Map<String,String> allFilter
                                        ) throws IOException {
        return mobileClient.getMobiles(allFilter);
    }

}
