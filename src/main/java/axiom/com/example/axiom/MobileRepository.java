package axiom.com.example.axiom;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface MobileRepository extends JpaRepository<Mobile, Integer>, JpaSpecificationExecutor<Mobile> {

}
