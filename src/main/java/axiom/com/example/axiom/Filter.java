package axiom.com.example.axiom;

public enum Filter {
    ID,
    BRAND,
    PHONE,
    PICTURE,
    SIM,
    RESOLUTION,
    AUDIOJACK,
    GPS,
    BATTERY,
    ANNOUNCEDATE,
    PRICEEUR,
    PRICE
}
