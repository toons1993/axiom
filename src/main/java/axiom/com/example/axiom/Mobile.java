package axiom.com.example.axiom;

import org.springframework.lang.NonNull;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Mobile {

    @Id
    private Integer id;

    @NonNull
    private String brand;

    @NonNull
    private String phone;

    @NonNull
    private String picture;

    @NonNull
    private String sim;

    @NonNull
    private String resolution;

    @NonNull
    @OneToOne(cascade = {CascadeType.ALL})
    private Hardware hardware;

    @OneToOne(cascade = {CascadeType.ALL})
    private Release release;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @NonNull
    public String getBrand() {
        return brand;
    }

    public void setBrand(@NonNull String brand) {
        this.brand = brand;
    }

    @NonNull
    public String getPhone() {
        return phone;
    }

    public void setPhone(@NonNull String phone) {
        this.phone = phone;
    }

    @NonNull
    public String getPicture() {
        return picture;
    }

    public void setPicture(@NonNull String picture) {
        this.picture = picture;
    }

    @NonNull
    public String getSim() {
        return sim;
    }

    public void setSim(@NonNull String sim) {
        this.sim = sim;
    }

    @NonNull
    public String getResolution() {
        return resolution;
    }

    public void setResolution(@NonNull String resolution) {
        this.resolution = resolution;
    }

    @NonNull
    public Hardware getHardware() {
        return hardware;
    }

    public void setHardware(@NonNull Hardware hardware) {
        this.hardware = hardware;
    }

    public Release getRelease() {
        return release;
    }

    public void setRelease(Release release) {
        this.release = release;
    }
}

