package axiom.com.example.axiom;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class MobileSpecification implements Specification<Mobile> {
    private SearchCriteria criteria;

    public MobileSpecification(SearchCriteria searchCriteria) {
        criteria = searchCriteria;
    }


    @Override
    public Predicate toPredicate
            (Root<Mobile> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        switch (criteria.getOperation()) {
            case MOBILE:
                switch (criteria.getKey()) {
                    case "id":
                        return cb.equal(
                                root.<String>get(criteria.getKey()), criteria.getValue().toString());
                    default:
                        return  cb.like(cb.lower(
                                        root.get(
                                                criteria.getKey()
                                        )
                                ), "%" + criteria.getValue().toString().toLowerCase() + "%"
                        );

                }
            case HARDWARE:
                Join<Mobile, Hardware> hardwareJoin = root.join("hardware");
                return cb.like(
                        hardwareJoin.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            case RELEASE:
                Join<Mobile, Release> releaseJoin = root.join("release");
                    switch (criteria.getKey()) {
                        case "priceEur":
                        case "price":
                            return cb.equal(
                                    releaseJoin.<String>get("priceEur"), criteria.getValue());
                        default:
                            return  cb.like(cb.lower(
                                releaseJoin.get(
                                        criteria.getKey()
                                )
                                ), "%" + criteria.getValue().toString().toLowerCase() + "%"
                        );
                    }
            default:
                throw new IllegalArgumentException("");
        }
    }
}
